<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Response;
use App\Patient;

class PatientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $patient = Patient::select("*")->orderBy('id', 'desc')->get();
        return view('admin.patients.index', compact('patient'));
    }
    public function create(){
        return view('admin.patients.create');
    }
    public function store(Request $request){
        $patient = new Patient;
        $patient->name = $request->name;
        $patient->surname = $request->surname;
        $patient->lastname = $request->lastname;
        $patient->documenttype = $request->documenttype;
        $patient->document = $request->document;
        $patient->admissiondate = $request->admissiondate;
        $patient->healthadministrator = $request->healthadministrator;
        $patient->authorization = $request->authorization;
        $patient->therapiesauth = $request->therapiesauth;
        $patient->constherapies = $request->constherapies;
        $patient->observation = $request->observation;
        $patient->save();
        return redirect('/admin/patients');
    }
    public function show($id){
    }
    public function edit($id){
        $patient = Patient::findOrFail($id);
        return view('admin.patients.update', compact('patient'));
    }
    public function update(Request $request, $id){
        $patient = Patient::findOrFail($id);
        $patient->name = $request->name;
        $patient->surname = $request->surname;
        $patient->lastname = $request->lastname;
        $patient->documenttype = $request->documenttype;
        $patient->document = $request->document;
        $patient->admissiondate = $request->admissiondate;
        $patient->healthadministrator = $request->healthadministrator;
        $patient->authorization = $request->authorization;
        $patient->therapiesauth = $request->therapiesauth;
        $patient->constherapies = $request->constherapies;
        $patient->observation = $request->observation;
        $patient->save();
        return Redirect::to('admin/patients');
    }
    public function destroy($id){
    }
}
