<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Response;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $user = User::select("*")->orderBy('id', 'desc')->get();
        return view('admin.usuarios.index', compact('user'));
    }
    public function create(){
        
    }
    public function store(Request $request){
        
    }
    public function show($id){
    }
    public function edit($id){
        
    }
    public function update(Request $request, $id){
        
    }
    public function destroy($id){
    }
}
