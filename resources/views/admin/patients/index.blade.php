@extends('layouts.app')
@section('content')
    <div class="row justify-content-center list_users">
        <div class="col-md-12">
            <div class="">
                <div class="title_users_list text-center"><h3>Lista de Terapias</h3></div>
                <div class="table-responsive card-body">
                    <div class="row justify-content-center pb-3">
                        <a href="{{ url('/admin/patients/create') }}" class="btn btn-primary">Nueva Terapia</a>
                    </div>
                    <table class="table table-hover">
                        <thead>
                            <th><b>Nombre</b></th>
                            <th><b>Documento de Identidad</b></th>
                            <th><b>Número de Autorización</b></th>
                            <th class="text-center"><b>Acciones</b></th>
                        </thead>
                        <tbody>
                            @foreach ($patient as $p)
                                <tr>
                                    <td>{{ $p->name.' '.$p->surname.' '.$p->lastname }}</td>
                                    <td>{{ $p->document }}</td>
                                    <td>{{ $p->authorization }}</td>
                                    <td class="text-center">  
                                        <a href="{{ url('/admin/patients/'.$p->id.'/edit') }}" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
