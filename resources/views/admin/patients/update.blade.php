@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <a href="{{ URL::to('/admin/patients') }}">
                        <i class="fa fa-hand-o-left"></i>
                    </a>
                    <h3 class="text-center">Editar Terapia</h3>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="">
                <div class="card-body">
                    <form action="{{ route('patients.update', $patient->id) }}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('name', 'Nombres:', ['class' => 'control-label']) !!}
                                    <input type="text" class="form-control" id="name" name="name" value="{{ $patient->name }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('surname', 'Primer Apellido:', ['class' => 'control-label']) !!}
                                    <input type="text" name="surname" id="surname" class="form-control" value="{{ $patient->surname }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('lastname', 'Segundo Apellido:', ['class' => 'control-label']) !!}
                                    <input type="text" name="lastname" id="lastname" class="form-control" value="{{ $patient->lastname }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('documenttype', 'Tipo de Documento:', ['class' => 'control-label']) !!}
                                    <select class="form-control" name="documenttype" id="documenttype">
                                        <option value="">-- Seleccione --</option>
                                        <option value="1" <?php if($patient->documenttype=='1'){ echo "selected"; } ?>>Cédula de Ciudadania</option>
                                        <option value="2" <?php if($patient->documenttype=='2'){ echo "selected"; } ?>>Pasaporte</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('document', 'Documento de Identidad:', ['class' => 'control-label']) !!}
                                    <input type="text" name="document" id="document" class="form-control" value="{{ $patient->document }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('admissiondate', 'Fecha Ingreso:', ['class' => 'control-label']) !!}
                                    <input type="date" name="admissiondate" id="admissiondate" class="form-control" value="{{ $patient->admissiondate }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('healthadministrator', 'Administradora de Salud:', ['class' => 'control-label']) !!}
                                    <input type="text" name="healthadministrator" id="healthadministrator" class="form-control" value="{{ $patient->healthadministrator }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('authorization', 'Número de Autorización:', ['class' => 'control-label']) !!}
                                    <input type="text" name="authorization" id="authorization" class="form-control" value="{{ $patient->authorization }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('therapiesauth', 'Número de Terapias Autorizadas:', ['class' => 'control-label']) !!}
                                    <input type="text" name="therapiesauth" id="therapiesauth" class="form-control" value="{{ $patient->therapiesauth }}"/>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4">
                                <div class="form-group">
                                    {!! Form::label('constherapies', 'Número Consecutivo de Terapias:', ['class' => 'control-label']) !!}
                                    <input type="text" name="constherapies" id="constherapies" class="form-control" value="{{ $patient->constherapies }}"/>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    {!! Form::label('observation', 'Observaciones:', ['class' => 'control-label']) !!}
                                    <textarea class="form-control" name="observation" id="observation">{{ $patient->observation }}</textarea>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <div class="justify-content-center">
                                    {!! Form::submit('Guardar Terapia', ['class' => 'btn btn-primary']) !!}

                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
