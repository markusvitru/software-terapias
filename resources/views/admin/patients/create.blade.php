@extends('layouts.app')
@section('content')
    {!! Form::open([
		'route' => 'patients.store',
        'method' => 'post'
	]) !!}
    <div class="row">
        <div class="col-12">
            <a href="{{ URL::to('/admin/patients') }}">
                <i class="fa fa-hand-o-left"></i>
            </a>
            <h3 class="text-center">Crear Terapia Física</h3>
        </div>
        <div class="col-12 col-sm-4">
            <div class="form-group">
                {!! Form::label('name', 'Nombres:', ['class' => 'control-label']) !!}
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
        </div>
        <div class="col-12 col-sm-4">
            <div class="form-group">
                {!! Form::label('surname', 'Primer Apellido:', ['class' => 'control-label']) !!}
                <input type="text" name="surname" id="surname" class="form-control"/>
            </div>
        </div>
        <div class="col-12 col-sm-4">
            <div class="form-group">
                {!! Form::label('lastname', 'Segundo Apellido:', ['class' => 'control-label']) !!}
                <input type="text" name="lastname" id="lastname" class="form-control"/>
            </div>
        </div>
        <div class="col-12 col-sm-4">
            <div class="form-group">
                {!! Form::label('documenttype', 'Tipo de Documento:', ['class' => 'control-label']) !!}
                <select class="form-control" name="documenttype" id="documenttype">
                    <option value="">-- Seleccione --</option>
                    <option value="1">Cédula de Ciudadania</option>
                    <option value="2">Pasaporte</option>
                </select>
            </div>
        </div>
        <div class="col-12 col-sm-4">
            <div class="form-group">
                {!! Form::label('document', 'Documento de Identidad:', ['class' => 'control-label']) !!}
                <input type="text" name="document" id="document" class="form-control"/>
            </div>
        </div>
        <div class="col-12 col-sm-4">
            <div class="form-group">
                {!! Form::label('admissiondate', 'Fecha Ingreso:', ['class' => 'control-label']) !!}
                <input type="date" name="admissiondate" id="admissiondate" class="form-control"/>
            </div>
        </div>
        <div class="col-12 col-sm-4">
            <div class="form-group">
                {!! Form::label('healthadministrator', 'Administradora de Salud:', ['class' => 'control-label']) !!}
                <input type="text" name="healthadministrator" id="healthadministrator" class="form-control"/>
            </div>
        </div>
        <div class="col-12 col-sm-4">
            <div class="form-group">
                {!! Form::label('authorization', 'Número de Autorización:', ['class' => 'control-label']) !!}
                <input type="text" name="authorization" id="authorization" class="form-control"/>
            </div>
        </div>
        <div class="col-12 col-sm-4">
            <div class="form-group">
                {!! Form::label('therapiesauth', 'Número de Terapias Autorizadas:', ['class' => 'control-label']) !!}
                <input type="text" name="therapiesauth" id="therapiesauth" class="form-control"/>
            </div>
        </div>
        <div class="col-12 col-sm-4">
            <div class="form-group">
                {!! Form::label('constherapies', 'Número Consecutivo de Terapias:', ['class' => 'control-label']) !!}
                <input type="text" name="constherapies" id="constherapies" class="form-control"/>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                {!! Form::label('observation', 'Observaciones:', ['class' => 'control-label']) !!}
                <textarea class="form-control" name="observation" id="observation"></textarea>
            </div>
        </div>
        <div class="col-12 text-center">
            <div class="justify-content-center">
                {!! Form::submit('Guardar Terapia', ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
