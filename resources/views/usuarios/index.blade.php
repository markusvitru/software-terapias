@extends('layouts.app')
@section('content')
    <div class="row justify-content-center list_users">
        <div class="col-md-12">
            <div class="">
                <div class="title_users_list text-center"><h3>Lista de Usuarios</h3></div>
                <div class="table-responsive card-body">
                    <div class="row justify-content-center pb-3">
                        <a href="{{ url('/auth/register') }}" class="btn btn-primary">Nuevo Usuario</a>
                    </div>
                    <table class="table table-hover">
                        <thead>
                            <th><b>Nombre</b></th>
                            <th><b>Email</b></th>
                            <th class="text-center"><b>Acciones</b></th>
                        </thead>
                        <tbody>
                            @foreach ($user as $u)
                                <tr>
                                    <td>{{ $u->name }}</td>
                                    <td>{{ $u->email }}</td>
                                    <td class="text-center">  
                                        <a href="{{ url('/usuarios/'.$u->id.'/edit') }}" class="btn btn-primary">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
